import falcon

from services.CitiesService import CitiesService
from static.MESSAGE_CONSTS_EN import *

class Cities(object):
    cached_cities = None

    def __init__(self, cities_service: CitiesService):
        self.cities_service = cities_service

    def on_get(self, _, resp):
        try:
            resp.status = falcon.HTTP_200
            resp.body = self.cities_service.get_top_cities_full_info_json()

        except Exception:
            #todo add log to wake engineers up
            resp.status = falcon.HTTP_500
            resp.body = GENERAL_FAILURE


