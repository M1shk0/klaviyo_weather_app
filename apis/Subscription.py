import json

import falcon
from validate_email import validate_email

from errors.CityError import CityError
from errors.EmailError import EmailError
from services.CitiesService import CitiesService
from services.SubscriptionService import SubscriptionService
from static.MESSAGE_CONSTS_EN import *


class Subscription(object):
    cached_cities = None

    subscription_service = None

    def __init__(self,
                 subscription_service: SubscriptionService,
                 cities_service: CitiesService):
        self.subscription_service = subscription_service
        self.cities_service = cities_service

    def on_post(self, req, resp):
        body = req.stream.read()
        try:
            subscription_data = json.loads(body.decode('utf-8'))

        except (ValueError, UnicodeDecodeError):
            raise falcon.HTTPError(falcon.HTTP_753,
                                   MALFORMED_JSON_SHORT_MESSAGE,
                                   MALFORMED_JSON_FULL_MESSAGE)
        try:
            self.validate_subscription_data(subscription_data)
        except KeyError:
            raise falcon.HTTPError(falcon.HTTP_400,
                                   MALFORMED_PAYLOAD_SHORT_MESSAGE,
                                   MALFORMED_PAYLOAD_SHORT_MISSING_DATA)
        except EmailError:
            raise falcon.HTTPError(falcon.HTTP_400,
                                   MALFORMED_PAYLOAD_SHORT_MESSAGE,
                                   MALFORMED_PAYLOAD_INVALID_EMAIL)
        except CityError:
            raise falcon.HTTPError(falcon.HTTP_400,
                                   MALFORMED_PAYLOAD_SHORT_MESSAGE,
                                   MALFORMED_PAYLOAD_INVALID_CITY)

        self.subscription_service.subscribe(subscription_data['city'], subscription_data['email'])
        resp.status = falcon.HTTP_200
        resp.body = json.dumps("Success")

    def on_delete(self, req, resp):
        body = req.stream.read()
        try:
            subscription_data = json.loads(body.decode('utf-8'))

        except (ValueError, UnicodeDecodeError):
            raise falcon.HTTPError(falcon.HTTP_753,
                                   MALFORMED_JSON_SHORT_MESSAGE,
                                   MALFORMED_JSON_FULL_MESSAGE)
        try:
            self.subscription_service.unsubscribe(subscription_data['email'])
        except KeyError:
            raise falcon.HTTPError(falcon.HTTP_400,
                                   MALFORMED_PAYLOAD_SHORT_MESSAGE,
                                   MALFORMED_PAYLOAD_INVALID_EMAIL)
        except Exception as e:
            print(e)
            raise falcon.HTTPError(falcon.HTTP_500,
                                   GENERAL_FAILURE)

        resp.status = falcon.HTTP_200
        resp.body = json.dumps(SUCCESS_RESPONSE)

    def validate_subscription_data(self, subscription_data):
        if not validate_email(subscription_data['email']):
            raise EmailError

        if not subscription_data['city'] in self.cities_service.get_top_cities_only_names():
            raise CityError
