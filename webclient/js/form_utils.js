function validate_form() {
    if (!_validate_city(document.getElementById("autocomplete").value)) {
        var results_html = document.getElementById("results")
        results_html.innerHTML = ""
        var errorMsg = document.createElement("li");
        errorMsg.innerHTML = "";
        errorMsg.setAttribute("class", "hintError");
        errorMsg.appendChild(document.createTextNode("We only allow top 100 cities, try typing closest biggest city"));
        results_html.appendChild(errorMsg);

        return false;
    }

    if (!_validate_email(document.getElementById("email").value)) {
        var results_html = document.getElementById("results")
        results_html.innerHTML = ""
        var errorMsg = document.createElement("li");
        errorMsg.innerHTML = "";
        errorMsg.setAttribute("class", "hintError");
        errorMsg.appendChild(document.createTextNode("Seem like your email isn't an email"));
        results_html.appendChild(errorMsg);

        return false;
    }

    return true;
}


function submit_form() {
     if (!validate_form()) {
        return;
     }

     var subscription_api = "/subscription"
     var xhr = new XMLHttpRequest();
     xhr.open("POST", subscription_api, true);
     xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

     var data = {
        email:document.getElementById("email").value,
        city:document.getElementById("autocomplete").value
     }

     xhr.send(JSON.stringify(data));
     xhr.onloadend = function () {
        var results_html = document.getElementById("results")
        results_html.innerHTML = ""
        var successMsg = document.createElement("li");
        successMsg.setAttribute("class", "hintSuccess");
        successMsg.appendChild(document.createTextNode("You have subscribed, check your inbox for info about whats happening outside :-)"));
        results_html.appendChild(successMsg);
     };
}


function _validate_city(city_state) {
    for (city in city_data) {
       if (city_state == city_data[city].fields.accentcity + " "
         + city_data[city].fields.region) {
            return true;
        }
    }
    return false;
}

function _validate_email(email) {
  var re = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
  return re.test(email);
}
