
CITIES_API_URL = "/cities";

city_data = {}
async function get_city_data() {
    response = await fetch(CITIES_API_URL);
    cities_obj = await response.json();
    city_data = cities_obj.records;
}


function get_location() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(show_closest_city);
  }
}

function show_closest_city(position) {
    var closest_city = pick_closest_city_for_latitude_longitude(position);
    autoCompleteVal.value = closest_city["accentcity"] + " " +  closest_city["region"];
}

function pick_closest_city_for_latitude_longitude(position) {
  var min_distance = 999999;
  var closest_city = {};
  for (city in city_data) {
     var city_lat = city_data[city].fields.latitude;
     var city_lon = city_data[city].fields.longitude;
     var crow_flies_dist = get_crow_flies(position.coords.latitude, position.coords.longitude, city_lat, city_lon)
     if (min_distance > crow_flies_dist) {
        min_distance = crow_flies_dist;
        closest_city = city_data[city];
     }
  }

  return closest_city.fields;
}


function get_crow_flies(lat1, lon1, lat2, lon2) {
  var earth_radius = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1);
  var a =
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
  var distance = earth_radius * c;
  return distance;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}

fuse = {};
get_city_data().then(()=>{
    get_location();
    var options = {
      shouldSort: false,
      matchAllTokens: true,
      threshold: 0.4,
      location: 0,
      distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      keys: ['fields.accentcity']
    };
    fuse = new Fuse(city_data, options)
})


populate_autocomplete = function() {
    var fuse_results = fuse.search(autoCompleteVal.value);
    var results_html = document.getElementById("results")
    results_html.innerHTML = ""
    if (fuse_results.length == 0 && autoCompleteVal.value.length > 1) {
        var li_el = document.createElement("li");
        li_el.setAttribute("class", "error");
        li_el.appendChild(document.createTextNode("As of now we only support top 100 cities by population \n" +
        "Please pick big city close to you."));
        results_html.appendChild(li_el);
    } else if (autoCompleteVal.value.length == 0) {
        var li_el = document.createElement("li");
        li_el.setAttribute("class", "hint");
        li_el.appendChild(document.createTextNode("Start typing your city name and select it from items bellow"));
        results_html.appendChild(li_el);
     }

    for (search_result in fuse_results) {
        var selected_city = fuse_results[search_result].fields.accentcity + " "
         + fuse_results[search_result].fields.region;
        var li_el = document.createElement("li");
        li_el.setAttribute("class", "city");

        li_el.appendChild(document.createTextNode(selected_city));
        li_el.onclick = function() {
            autoCompleteVal.value = this.innerHTML;
        }
        results_html.appendChild(li_el);
    }
};



var autoCompleteVal = document.getElementById("autocomplete");
autoCompleteVal.onkeyup = populate_autocomplete;
