MALFORMED_JSON_SHORT_MESSAGE = 'Malformed JSON'
MALFORMED_JSON_FULL_MESSAGE = 'Could not decode the request body. The '
'JSON was incorrect or not encoded as '
'UTF-8.'

MALFORMED_PAYLOAD_SHORT_MESSAGE = 'Invalid payload'
MALFORMED_PAYLOAD_SHORT_MISSING_DATA = 'missing mandatory data(email or city)'

MALFORMED_PAYLOAD_INVALID_EMAIL = 'Invalid email'
MALFORMED_PAYLOAD_INVALID_CITY = 'Invalid city'

GENERAL_FAILURE = 'Something went wrong'

SUCCESS_RESPONSE = 'OK'