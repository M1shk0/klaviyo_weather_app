from errors.WeatherAppError import WeatherAppError


class TomorrowsWeatherError(WeatherAppError):
    pass