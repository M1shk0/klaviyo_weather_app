from errors.WeatherAppError import WeatherAppError


class CurrentWeatherError(WeatherAppError):
    pass