from abc import abstractmethod

from clients.dto.Weather import Weather


class AbstractWeather:

    @abstractmethod
    def get_current_weather(self, lat: str, lon: str) -> Weather:
        pass

    @abstractmethod
    def get_tomorrows_weather(self, lat: str, lon: str)-> Weather:
        pass
