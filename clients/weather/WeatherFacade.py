from clients.weather.AbstractWeather import AbstractWeather
from errors import CurrentWeatherError
from errors.TomorrowsWeatherError import TomorrowsWeatherError


class WeatherFacade(AbstractWeather):

    def __init__(self, weather_configs: dict, logger):
        self.strategies = []
        for weather_class, api_key in weather_configs.items():
            self.strategies.append(weather_class(api_key))
        self.logger = logger

    def get_tomorrows_weather(self, lat: str, lon: str):
        for strategy in self.strategies:
            result = strategy.get_tomorrows_weather(lat, lon)
            if result:
                return result
        raise TomorrowsWeatherError

    def get_current_weather(self, lat: str, lon: str):
        for strategy in self.strategies:
            try:
                result = strategy.get_current_weather(lat, lon)
                if result:
                    return result
            except Exception as e:
                self.logger.warning(str(e))

        raise CurrentWeatherError
