import requests

from clients.dto.Weather import Weather
from clients.weather.AbstractWeather import AbstractWeather


class WeatherBit(AbstractWeather):
    current_weather_uri = "https://api.weatherbit.io/v2.0/current?lat=%s&lon=%s&key=%s&units=I"

    forecasted_weather_uri = "https://api.weatherbit.io/v2.0/forecast/daily?lat=%s&lon=%s&days=2&units=I&key=%s"

    def __init__(self, key):
        self.api_key = key

    def get_tomorrows_weather(self, lat: str, lon: str) -> Weather:
        result = requests.get(self.forecasted_weather_uri % (lat, lon, self.api_key))
        parsed_weather = result.json()
        return Weather(parsed_weather["data"][1]["temp"], parsed_weather["data"][1]["precip"],
                       parsed_weather["data"][1]["weather"]['description'])

    def get_current_weather(self, lat: str, lon: str) -> Weather:
        result = requests.get(self.current_weather_uri % (lat, lon, self.api_key))
        parsed_weather = result.json()

        return Weather(parsed_weather["data"][0]["temp"], parsed_weather["data"][0]["precip"],
                       parsed_weather["data"][0]["weather"]['description'])
