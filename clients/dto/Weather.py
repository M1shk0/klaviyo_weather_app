class Weather:
    def __init__(self, temp, precipitation, description):
        self.temp = int(temp)
        self.precipitation = precipitation
        self.description = description
