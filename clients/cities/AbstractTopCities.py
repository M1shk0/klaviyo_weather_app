from abc import abstractmethod


#
# TODO:
# to really support multiple clients:
# we would need to add parse method and have DTO object that will collect the data in unified format
class AbstractTopCities:

    @abstractmethod
    def getTopCities(self, count_of_cities) ->str:
        pass