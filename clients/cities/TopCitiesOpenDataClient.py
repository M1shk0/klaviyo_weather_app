import requests

from clients.cities.AbstractTopCities import AbstractTopCities


class TopCitiesOpenDataClient(AbstractTopCities):
    url = "https://public.opendatasoft.com/api/records/1.0/search/?dataset=worldcitiespop&rows=%d&sort=population&facet=country&refine.country=us"

    def getTopCities(self, count_of_cities) -> str:
        return requests.get(self.url % count_of_cities).text
