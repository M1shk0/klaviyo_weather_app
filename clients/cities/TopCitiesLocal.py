import requests

from clients.cities.AbstractTopCities import AbstractTopCities


class TopCitiesLocal(AbstractTopCities):
    file_path = "../static/cities.json"

    def getTopCities(self, city_count) -> str:
        # if its more then city_count not a big deal its less work for us to read the whole file
        # rather then parse it
        with open(self.file_path, 'r') as content_file:
            content = content_file.read()
        return content
