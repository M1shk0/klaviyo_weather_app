from clients.cities.AbstractTopCities import AbstractTopCities
from clients.cities.TopCitiesLocal import TopCitiesLocal
from clients.cities.TopCitiesOpenDataClient import TopCitiesOpenDataClient
from errors.CityError import CityError


class CitiesFacade(AbstractTopCities):

    def __init__(self, logger):
        self.strategies = []
        self.strategies.append(TopCitiesOpenDataClient())
        self.strategies.append(TopCitiesLocal())
        self.logger = logger

    def getTopCities(self, count_of_cities) -> str:
        for strategy in self.strategies:
            try:
                cities = strategy.getTopCities(count_of_cities)
                if cities:
                    return cities
            except Exception as e:
                self.logger.warning(str(e))

        raise CityError
