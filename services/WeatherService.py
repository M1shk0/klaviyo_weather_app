import pickle

from redis import StrictRedis

from clients.dto.Weather import Weather
from clients.weather import WeatherFacade
from services.CitiesService import CitiesService
import datetime


class WeatherService:

    #weather redis key will be weather:boston_ma:09_26 (moth_year)
    WEATHER_REDIS_PREFIX = ":weather:"

    DAY_EXPIRATION = 24 * 3600

    def __init__(self,
                 storage: StrictRedis,
                 weather_facade: WeatherFacade,
                 city_service: CitiesService,
                 app_prefix: str):
        self.storage = storage
        self.weather_static_cache = {}
        self.city_service = city_service
        self.weather_client = weather_facade
        self.app_prefix = app_prefix

    def get_todays_weather(self, city)-> Weather:
        weather_cache_key = self.app_prefix + self.WEATHER_REDIS_PREFIX + city + ":" + str(datetime.date.today())
        if weather_cache_key in self.weather_static_cache:
            return self.weather_static_cache.get(weather_cache_key)
        pickled_weather_from_redis = self.storage.get(weather_cache_key)

        if not pickled_weather_from_redis:
            cities_info = self.city_service.get_city_info(city)
            today_weather_from_client = self.weather_client.get_current_weather(cities_info["latitude"],
                                                                                cities_info["longitude"])
            self.storage.setex(weather_cache_key, self.DAY_EXPIRATION, pickle.dumps(today_weather_from_client))
            self.weather_static_cache[weather_cache_key] = today_weather_from_client
        else:
            today_weather_from_redis = pickle.loads(pickled_weather_from_redis)
            self.weather_static_cache[weather_cache_key] = today_weather_from_redis

        return self.weather_static_cache.get(weather_cache_key)

    def get_tomorrows_weather(self, city) -> Weather:
        tomorrow = datetime.date.today() + datetime.timedelta(days=1)
        weather_cache_key = self.app_prefix + self.WEATHER_REDIS_PREFIX + city + ":" + str(tomorrow)
        if weather_cache_key in self.weather_static_cache:
            return self.weather_static_cache.get(weather_cache_key)
        pickled_weather_from_redis = self.storage.get(weather_cache_key)

        if not pickled_weather_from_redis:
            cities_info = self.city_service.get_city_info(city)
            tomorrow_weather_from_client = self.weather_client.get_tomorrows_weather(cities_info["latitude"],
                                                                                     cities_info["longitude"])
            self.storage.setex(weather_cache_key, self.DAY_EXPIRATION, pickle.dumps(tomorrow_weather_from_client))
            self.weather_static_cache[weather_cache_key] = tomorrow_weather_from_client
        else:
            tomorrow_weather_from_redis = pickle.loads(pickled_weather_from_redis)
            self.weather_static_cache[weather_cache_key] = tomorrow_weather_from_redis

        return self.weather_static_cache.get(weather_cache_key)
