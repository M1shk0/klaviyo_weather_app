import json

from redis import StrictRedis

from clients.cities.CitiesFacade import CitiesFacade

class CitiesService:
    CITIES_JSON_REDIS_KEY = "cities_json"

    def __init__(self, storage: StrictRedis,
                 cities_client: CitiesFacade,
                 count_of_cities_to_pull: int,
                 app_prefix: str):
        self.storage = storage
        self.cities_info_json = None
        self.supported_cities_names = set()
        self.cities_client = cities_client
        self.cities_count = count_of_cities_to_pull
        self.app_prefix = app_prefix

    def get_top_cities_full_info(self):
        parsed_cities_info = json.loads(self.get_top_cities_full_info_json())
        return parsed_cities_info

    def get_top_cities_full_info_json(self):
        if self.cities_info_json:
            return self.cities_info_json

        cached_in_redis = self.storage.get(self.app_prefix + ":" + self.CITIES_JSON_REDIS_KEY)
        if not cached_in_redis:
            cached_in_redis = self.cities_client.getTopCities(self.cities_count)
            self.storage.set(self.app_prefix + ":" + self.CITIES_JSON_REDIS_KEY,
                             cached_in_redis)

        self.cities_info_json = cached_in_redis

        return self.cities_info_json

    def get_top_cities_only_names(self):
        if self.supported_cities_names:
            return self.supported_cities_names

        city_counter = 0
        for city_record in self.get_top_cities_full_info()['records']:
            if city_counter > self.cities_count:
                break
            self.supported_cities_names.add(city_record['fields']['accentcity'] + " " + city_record['fields']['region'])
            city_counter += 1
        return self.supported_cities_names

    def get_city_info(self, city_state: str):
        for city_record in self.get_top_cities_full_info()['records']:
            city_state_record = city_record['fields']['city'] + " " + city_record['fields']['region']
            if self.serialize_city_state_name(city_state_record) == city_state:
                return city_record['fields']

        return None

    @staticmethod
    def serialize_city_state_name(city_state_name):
        return city_state_name.replace(" ", "_").lower()
