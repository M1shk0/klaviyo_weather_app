from redis import StrictRedis
from clients.dto.Weather import Weather
import datetime


class EmailService:
    email_template_path = 'static/email_templates/weather.htm'

    def __init__(self):
        self.subject_local_cache = {}
        self.body_local_cache = {}
        self.today = str(datetime.date.today())

    def get_subject(self, weather_today: Weather, weather_tomorrow: Weather, city: str):
        subject_key = "subject:" + city + ":" + self.today
        if subject_key in self.subject_local_cache:
            return self.subject_local_cache.get(subject_key)

        temp_diff = weather_today.temp - weather_tomorrow.temp
        subject = "Enjoy a discount on us"
        if weather_today.precipitation == 0 or temp_diff >= 5:
            subject = "It's nice out! " + subject
        elif temp_diff <= -5 or weather_today.precipitation > 0:
            subject = "Not so nice out? That's okay, e" + subject[1:]

        self.subject_local_cache[subject_key] = subject
        return subject

    def get_body(self, weather_today: Weather, city: str):
        body_key = "body:" + city + ":" + self.today
        if body_key in self.subject_local_cache:
            return self.body_local_cache.get(body_key)

        with open(self.email_template_path, 'r') as content_file:
            content = content_file.read()

        content = content.replace("#city", city)
        content = content.replace("#curr_temp", str(weather_today.temp))
        content = content.replace("#description", str(weather_today.description))

        return content
