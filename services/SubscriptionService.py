from redis import StrictRedis

from services.CitiesService import CitiesService


class SubscriptionService:

    CITY_SUBSCRIBERS = "city:"

    USER_SUBSCRIBERS = "user_subscribed:"

    def __init__(self, storage: StrictRedis, app_prefix: str):
        self.storage = storage
        self.app_prefix = app_prefix

    def subscribe(self, city: str, email: str):
        pipe = self.storage.pipeline()
        city = self.storage_prep_city(city)
        pipe.sadd(self.app_prefix + ":" + self.CITY_SUBSCRIBERS + city, email)
        pipe.sadd(self.app_prefix + ":" + self.USER_SUBSCRIBERS + email, city)
        pipe.execute()

    def unsubscribe(self, email: str):
        all_cities_subscribed = self.storage.smembers(self.app_prefix + ":" + self.USER_SUBSCRIBERS + email)
        pipe = self.storage.pipeline()
        for city in all_cities_subscribed:
            decoded_city = self.storage_prep_city(city.decode('utf-8'))
            pipe.srem(self.app_prefix + ":" + self.CITY_SUBSCRIBERS + decoded_city, email)
        pipe.delete(self.app_prefix + ":" + self.USER_SUBSCRIBERS + email)
        pipe.execute()

    def get_all_subscribers_per_city(self, city: str):
        return self.storage.smembers(self.app_prefix + ":" + self.CITY_SUBSCRIBERS + self.storage_prep_city(city))

    def get_subscribers_for_city_in_batch_of(self, city: str, n: int):
        cursor, subscribers = self.storage.sscan(self.app_prefix + ":" + self.CITY_SUBSCRIBERS +
                                                CitiesService.serialize_city_state_name(city), 0, None, n)
        while cursor != 0:
            yield subscribers
            cursor, subscribers = self.storage.sscan(self.app_prefix + ":" + self.CITY_SUBSCRIBERS +
                                                    CitiesService.serialize_city_state_name(city), cursor, None, n)

        yield subscribers

    @staticmethod
    def storage_prep_city(city: str) -> str:
        return city.lower().replace(" ", "_")
