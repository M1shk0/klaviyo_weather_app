import unittest
from clients.dto.Weather import Weather
from services.EmailService import EmailService



class TestEmailServiceGetSubject(unittest.TestCase):

    def test_get_nice_subject(self):
        email_service = EmailService()
        # to have nice subject it must not rain and precipitation should be 0
        weather_today = Weather(90, 0, "sunny")
        weather_tomorrow = Weather(82, 0, "sunny")
        nice_subject = email_service.get_subject(weather_today, weather_tomorrow, "Boston")
        self.assertEqual(nice_subject, "It's nice out! Enjoy a discount on us")

    def test_get_not_nice_subject_raining_today(self):
        email_service = EmailService()
        weather_today = Weather(97, 1, "sunny")
        weather_tomorrow = Weather(95, 0, "sunny")
        nice_subject = email_service.get_subject(weather_today, weather_tomorrow, "Boston")
        self.assertEqual(nice_subject, "Not so nice out? That's okay, enjoy a discount on us")

    def test_get_nice_subject_cooler_but_sunny_today(self):
        email_service = EmailService()
        weather_today = Weather(90, 0, "sunny")
        weather_tomorrow = Weather(95, 0, "sunny")
        nice_subject = email_service.get_subject(weather_today, weather_tomorrow, "Boston")
        self.assertEqual(nice_subject, "It's nice out! Enjoy a discount on us")

if __name__ == '__main__':
    unittest.main()