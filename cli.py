import logging
from threading import Thread
from queue import Queue

import redis
import click

from clients.cities.CitiesFacade import CitiesFacade
from clients.weather.WeatherBit import WeatherBit
from clients.weather.WeatherFacade import WeatherFacade

from services.EmailService import EmailService
from services.SubscriptionService import SubscriptionService
from services.CitiesService import CitiesService
from services.WeatherService import WeatherService

from email.mime.text import MIMEText
import smtplib

APP_NAME = "klaviyo_weather"
SYSTEM_EMAIL = "misha@web2u.org"
logger = logging.getLogger(APP_NAME)

# this batch size is for COUNT in SSCAN which is "recommendation of how much work redis should do"
BATCH_SIZE = 100

SMTP_HOSTNAME = "0.0.0.0"
SMTP_PORT = 25

storage = redis.StrictRedis(host="0.0.0.0", port=6379, db=0)

NUMBER_EMAILER_THREADS = 3

weather_config = {WeatherBit: "3de96c6c94714d07ba4ad666d65914d7"}
weather_client = WeatherFacade(weather_config, logger)
cities_client = CitiesFacade(logger)

cities_service = CitiesService(storage, cities_client, 100, APP_NAME)
weather_service = WeatherService(storage, weather_client, cities_service, APP_NAME)
subscription_service = SubscriptionService(storage, APP_NAME)
email_service = EmailService()

email_queue = Queue()


@click.group()
def main():
    setup_threads(email_queue)


@main.command()
@click.argument('cities_csv')
def process_csv(cities_csv: str):
    cities_splitted = cities_csv.split(",")
    process_cities_subscribers(cities_splitted)
    email_queue.join()

@main.command()
def process_all():
    all_cities = cities_service.get_top_cities_only_names()
    map(CitiesService.serialize_city_state_name, all_cities)
    process_cities_subscribers(all_cities)
    email_queue.join()


def process_cities_subscribers(cities: list):
    for city in cities:
        print("Working on city " + city)
        city_subscribers = 0
        city = CitiesService.serialize_city_state_name(city)
        today_weather = None
        tomorrow_weather = None

        for subscribers in subscription_service.get_subscribers_for_city_in_batch_of(city, BATCH_SIZE):
            if not subscribers:
                continue
            if subscribers and not today_weather:
                today_weather = weather_service.get_todays_weather(city)
                tomorrow_weather = weather_service.get_tomorrows_weather(city)

            while subscribers:
                subscriber = subscribers.pop()
                email_queue.put((subscriber, today_weather, tomorrow_weather, city))
                city_subscribers += 1
        print("Processed %d subscribers" % city_subscribers)


def setup_threads(queue: Queue):
    for i in range(NUMBER_EMAILER_THREADS):
        worker = Thread(target=send_email, args=(queue,))
        worker.setDaemon(True)
        worker.start()


def send_email(queue: Queue):
    while True:
        subscriber, today_weather, tomorrow_weather, city = queue.get()
        city_info = cities_service.get_city_info(city)
        message = MIMEText(email_service.get_body(today_weather, city_info['accentcity'] + " " + city_info['region']))
        message['Subject'] = email_service.get_subject(today_weather, tomorrow_weather, city)
        message['From'] = SYSTEM_EMAIL
        message['To'] = subscriber.decode('utf-8')
        with smtplib.SMTP(SMTP_HOSTNAME, SMTP_PORT) as server:
            server.ehlo()  # Can be omitted
            server.sendmail(message['From'],[message['To']], message.as_string())
        queue.task_done()


if __name__ == "__main__":
    main()
