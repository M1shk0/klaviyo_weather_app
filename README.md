# Klaviyo Weather app
## How to run
1. Verify that you have redis running 
2. Validate that all the credentials in app.py are correct
(redis host and port)
    * in the future we will move them out of python into env variables
    that gets populated from secret file based on server
3. Ensure that you are using python3 and pip3 

run:

 * _pip install -r requirements.txt_
 
 * _gunicorn app:app_
 
That will spin up our server on port 8000
from that point on you should be able to navigate to 
* To see webClient http://127.0.0.1:8000/webclient/index.html
* To verify backend is working http://127.0.0.1:8000/cities
 
## Backend technologies used
Backend is built as completely standalone REST service.

* As a Language I've chosen python3(in my day job I use Java but I really wanted
to dive deep in python world for a change)

* As a framework I've chosen Falcon(https://falcon.readthedocs.io/en/stable/) 
Obviously any kind of framework is a reason for debate but I liked working with
falcon for its speed and cleanliness. Also I feel like Falcon provides just enough
and not too much

* As a database I've chosen Redis, mostly for the same reason as I've chosen python - 
redis is a fun technology to work with and allows for super fast transactions(piping 
commands together) 

## Backend API layer
For our app we will have only 2 controllers:

* Subscription 

  * POST to create subscription to the city (See  Subscription.on_post)
  
  * DELETE to unsubscribe (See Subscription.on_delete)
  
* Cities

  * GET to get top cities as json with all the information(See Cities.on_get)
  
For subscription controller we validate the data (email, city right in the controller)
(see Subscription.validate_subscription_data).
That is ok since we pass only 2 fields, if at some point this payload will grow we will 
consider implementing custom DTO to pass to validator object which should injected through
DI.

## Backend DB Layer
To handle subscriptions per user we utilise Redis Sets.
We have following sets to handle all the possible cases:

* ...city:{cityName} contains Email list (list of subscribers per city)

* ...user_subscribed:{email} contains cities that subscriber subscribed to(so we can unsubscribe him later)

For subscription and unsubscribe we utilize pipes which saves us trips back and forth with
redis

Besides utilising Redis as DB we also use it as secondary(first is local dictionary) cache 
for most of things(weather per city, subject per city, city list, etc.)

## Clients Architecture
In clients directory we have our implementation for weather and cities clients
I built it the way so  we could easily add more clients implementations in the future
One thing to improve there is to return custom object from CitiesFacade rather then rely
on one API.

## CLI
For cli utility I've used Click framework, which allows for rapid and 
beautiful(from the outside) development of cli utils.
There couple of options of how to run this util:

* _python cli.py process-all_ 

* _python cli.py process-csv  "boston ma,washington dc,San Diego CA"_

First one will run for all the cities that we allow users to subscribe to.
Second will only run for cities specified, this way we can spread load in between 
multiple instances.

I've pushed all the email sending to separate pool of threads with a queue.
this way we can scan our redis set independently from sending emails. In the
future we might think about using some kind of broker(ActiveMq or smth)

# Tests
Not much to say there besides the fact that this codeBase needs more tests.
So far I have added one test to show my understanding around email subject generation - 
see TestEmailServiceGetSubject.

## FRONTEND
For simplicity and speed I've decided to stay away from any framework at that
early version. 
Frontend is represented by single  html file - index.html that falcon serves it 
as static content. In the future that can be refactored into Vue.js app

How does it work:

1. Once Html loads first thing we do is pull cities_list from our server and store in 
global variable to be used throughout js.

2. We ask for Geo permission 

    1. If granted we figure out the closest city from the city_list based on crow_flies algorithm
    
3. If user decided not to allow for Geo perm or he wants to type city himself 3'd party library called 
Fuse will help us with searching and populating suggestions for cities to use   

4. On submit we validate form and submit it with ajax to our server see form_utils.js


 



