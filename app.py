import logging
import os

import falcon
import redis
from apis.Cities import Cities
from apis.Subscription import Subscription
from clients.cities.CitiesFacade import CitiesFacade
from services.CitiesService import CitiesService
from services.SubscriptionService import SubscriptionService

APP_NAME = "klaviyo_weather"
COUNT_OF_CITIES_TO_PULL = 100

app = falcon.API()

logger = logging.getLogger(APP_NAME)

#will use redis as our db
storage = redis.StrictRedis(host="0.0.0.0", port=6379, db=0)

#city client to pull top cities
cities_client = CitiesFacade(logger)

#services
subscription_service = SubscriptionService(storage, APP_NAME)
cities_service = CitiesService(storage, cities_client, COUNT_OF_CITIES_TO_PULL, APP_NAME)

# Resources are represented by long-lived class instances
cities = Cities(cities_service)
subscription = Subscription(subscription_service, cities_service)

app.add_route('/subscription', subscription)
app.add_route('/cities', cities)

#static webclient
path_to_html = os.getcwd()+'/webclient'
app.add_static_route('/webclient', path_to_html)
